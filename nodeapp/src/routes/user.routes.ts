import { Router } from "express";
import {
	createUserHandler, 
	getUserHandler,
	updateUserHandler,
	deleteUserHandler
} from "../controller/user.controller";
import requireUser from "../middleware/requireUser";
import validateResource from "../middleware/validateResource";
import { createUserSchema, updateUserSchema } from "../schema/user.schema";

const router = Router({ mergeParams: true });

router.route("/").post(validateResource(createUserSchema), createUserHandler)
	.get([requireUser], getUserHandler)
	.put([requireUser, validateResource(updateUserSchema)], updateUserHandler)
	.delete([requireUser], deleteUserHandler);
export default router;
