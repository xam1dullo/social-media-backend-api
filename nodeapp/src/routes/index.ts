import { Router } from "express";

import commentRoutes from "./comment.routes";
import userRoutes from "./user.routes";
import postRoutes from "./post.routes";
import sessionRoutes from "./session.routes";
import healthcheckRoutes from "./healthcheck.routes";

const router = Router({ mergeParams: true });

router.use("/users", userRoutes);

router.use("/sessions", sessionRoutes);

router.use("/posts", postRoutes);

router.use("/posts/:postId/comments", commentRoutes);

router.use("/healthcheck", healthcheckRoutes);

export default router;
