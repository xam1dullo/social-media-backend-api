import { Router } from "express";
import requireUser from "../middleware/requireUser";
import validateResource from "../middleware/validateResource";
import {
    createCommentHandler,
    updateCommentHandler,
    deleteCommentHandler,
    getCommentHandler,
    getCommentsHandler,
} from "../controller/comment.controller";
import {
    createCommentSchema,
    updateCommentSchema,
    deleteCommentSchema,
    getCommentSchema,
} from "../schema/comment.schema";

const router = Router({ mergeParams: true });

router
    .route("/")
    .post(
        [requireUser, validateResource(createCommentSchema)],
        createCommentHandler
    )
    .get(
        [requireUser],
        getCommentsHandler
    );

router
    .route("/:commentId")
    .get([requireUser, validateResource(getCommentSchema)], getCommentHandler)
    .put(
        [requireUser, validateResource(updateCommentSchema)],
        updateCommentHandler
    )
    .delete(
        [requireUser, validateResource(deleteCommentSchema)],
        deleteCommentHandler
    );
export default router;
