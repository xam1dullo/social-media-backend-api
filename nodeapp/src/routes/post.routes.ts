import { Router } from "express";
import requireUser from "../middleware/requireUser";
import validateResource from "../middleware/validateResource";
import {
    createPostHandler,
    updatePostHandler,
    deletePostHandler,
    getPostHandler,
    getPostsHandler,
} from "../controller/post.controller";
import {
    createPostSchema,
    updatePostSchema,
    deletePostSchema,
    getPostSchema,
} from "../schema/post.schema";

const router = Router({ mergeParams: true });

router
    .route("/")
    .post([requireUser, validateResource(createPostSchema)], createPostHandler)
    .get([requireUser], getPostsHandler)

router
    .route("/:postId")
    .get([requireUser, validateResource(getPostSchema)], getPostHandler)
    .put([requireUser, validateResource(updatePostSchema)], updatePostHandler)
    .delete(
        [requireUser, validateResource(deletePostSchema)],
        deletePostHandler
    );
export default router;
