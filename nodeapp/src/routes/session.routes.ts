import { Router } from "express";
import {
    createUserSessionHandler,
    getUserSessionsHandler,
    deleteSessionHandler,
} from "../controller/session.controller";
import requireUser from "../middleware/requireUser";
import validateResource from "../middleware/validateResource";
import { createSessionSchema } from "../schema/session.schema";

const router = Router({ mergeParams: true });

router
    .route("/")
    .post(validateResource(createSessionSchema), createUserSessionHandler)
    .get(requireUser, getUserSessionsHandler)
    .delete(requireUser, deleteSessionHandler);

export default router;
