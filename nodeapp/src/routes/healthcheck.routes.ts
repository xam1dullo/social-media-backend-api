import { Router } from "express";

const router = Router({ mergeParams: true });
import requireUser from "../middleware/requireUser";

// TODO: check
router
    .route("/")
    //@ts-ignore
    .get(requireUser, (_req: Request, res: Response) => res.sendStatus(200));

export default router;
