import { Express } from "express";
import dotenv from "dotenv";
dotenv.config();
import config from "config";
import connect from "./utils/connect";
import logger from "./utils/logger";
import routes from "./routes";
import createServer from "./utils/server";
import { startMetricsServer } from "./utils/metrics";

const port: number = config.get<number>("port");
export const app: Express = createServer();

app.listen(port, async () => {
    logger.info(`App is running at http://localhost:${port}`);

    await connect();
    startMetricsServer()
    routes(app);

});
