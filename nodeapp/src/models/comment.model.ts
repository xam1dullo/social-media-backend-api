import mongoose from "mongoose";
import { customAlphabet } from "nanoid";
import { UserDocument } from "./user.model";
import { PostDocument } from "./post.model";
const nanoid = customAlphabet("abcdefghijklmnopqrstuvwxyz0123456789", 10);

export interface CommentInput {
    user: UserDocument["_id"];
    postId: PostDocument["_id"];
    comment: string;
    commentId?: string;
}

export interface CommentDocument extends CommentInput, mongoose.Document {
    createdAt: Date;
    updatedAt: Date;
}

const commentSchema = new mongoose.Schema(
    {
        commentId: {
            type: String,
            required: true,
            unique: true,
            default: () => `comment_${nanoid()}`,
        },
        comment: { type: String, required: true },
        postId: { type: mongoose.Schema.Types.String, ref: "User" },
        user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    },
    {
        timestamps: true,
    }
);

commentSchema.post<CommentDocument>("save", async function (commet: any) {
    const post = await mongoose.model<PostDocument>("Post").findOne({ postId: commet.postId });
    console.log({ post, commet })
    if (post && post.comments) {
        post.comments.push(commet.commentId);
        await post.save();
    }
});

const ProductModel = mongoose.model<CommentDocument>("Comment", commentSchema);

export default ProductModel;
