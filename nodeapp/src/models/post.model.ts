import mongoose from "mongoose";
import { customAlphabet } from "nanoid";
import { CommentDocument } from "./comment.model";

const nanoid = customAlphabet("abcdefghijklmnopqrstuvwxyz0123456789", 10);

export interface PostInput {
    user: CommentDocument["_id"];
    title: string;
    body: string;
    postId?: string;
    comments?: Array<string>;
}

export interface PostDocument extends PostInput, mongoose.Document {
    createdAt: Date;
    updatedAt: Date;
}

const postSchema = new mongoose.Schema(
    {
        postId: {
            type: String,
            required: true,
            unique: true,
            default: () => `post_${nanoid()}`,
        },
        user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
        title: { type: String, required: true },
        body: { type: String, required: true },
        comments: [
            {
                type: mongoose.Schema.Types.String,
                ref: "Comment",
            },
        ],
    },
    {
        timestamps: true,
    }
);



const PostModel = mongoose.model<PostDocument>("Post", postSchema);

export default PostModel;
