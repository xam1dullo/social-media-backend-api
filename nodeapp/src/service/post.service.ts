import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import ProstModel, { PostDocument, PostInput } from "../models/post.model";

interface SortQuery extends Record<string, number> {}
export async function createPost(input: PostInput) {
    try {
        const result: unknown = await ProstModel.create(input);
        return result;
    } catch (e) {
        throw e;
    }
}
export async function findPosts(page: number = 1, limit: number = 10) {
    try {
        const skip = (page - 1) * limit;
        const posts = await ProstModel.find({}).skip(skip).limit(limit);
        return posts;
    } catch (e) {
        throw e;
    }
}

export async function findPost(
    query: FilterQuery<PostDocument>,
    options: QueryOptions = { lean: true }
) {
    try {
        const post = await ProstModel.findOne(query, {}, options);
        return post;
    } catch (e) {
        throw e;
    }
}

export async function findAndUpdatePost(
    query: FilterQuery<PostDocument>,
    update: UpdateQuery<PostDocument>,
    options: QueryOptions
) {
    return await ProstModel.findOneAndUpdate(query, update, options);
}

export async function deletePost(query: FilterQuery<PostDocument>) {
    return await ProstModel.deleteOne(query);
}
