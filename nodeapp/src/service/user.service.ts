import { FilterQuery, UpdateQuery, QueryOptions } from "mongoose";
import { omit } from "lodash";
import UserModel, { UserDocument, UserInput } from "../models/user.model";

export async function createUser(input: UserInput) {
    try {
        const user = await UserModel.create(input);

        return omit(user.toJSON(), "password");
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function validatePassword({
    email,
    password,
}: {
    email: string;
    password: string;
}) {
    const user = await UserModel.findOne({ email });

    if (!user) {
        return false;
    }

    const isValid = await user.comparePassword(password);

    if (!isValid) return false;

    return omit(user.toJSON(), "password");
}


export async function findUser(
    query: FilterQuery<UserDocument>,
    update: UpdateQuery<UserDocument>,
    options: QueryOptions
) {
    try {
        const user = await UserModel.findOne(query, {}, options);
        return user;
    } catch (e) {
        throw e;
    }
}

export async function findUserForSession(query: FilterQuery<UserDocument>) {
    return UserModel.findOne(query).lean();
}

export async function updateUser(
    query: FilterQuery<UserDocument>,
    update: UpdateQuery<UserDocument>,
    options?: QueryOptions
) {
    const user = await UserModel.findOneAndUpdate(query, update, options)
    return user
}

export async function deleteUser(query: FilterQuery<UserDocument>) {
    return await UserModel.deleteOne(query);
}
