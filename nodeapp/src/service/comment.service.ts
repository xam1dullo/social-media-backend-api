import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import CommentModel, {
    CommentDocument,
    CommentInput,
} from "../models/comment.model";

export async function createComment(input: CommentInput) {
    try {
        const comment = await CommentModel.create(input);
        console.log(comment)
        return comment;
    } catch (e) {
        throw e;
    }
}

export async function findComments(postId: string, page: number = 1, limit: number = 10) {
    try {
        const skip = (page - 1) * limit;
        const comments = await CommentModel.find({ postId }).skip(skip).limit(limit);
        return comments;
    } catch (e) {
        throw e;
    }
}
export async function findComment(
    query: FilterQuery<CommentDocument>,
    options: QueryOptions = { lean: true }
) {
    try {
        const comment = await CommentModel.findOne(query, {}, options);
        return comment;
    } catch (e) {
        throw e;
    }
}

export async function findAndUpdateComment(
    query: FilterQuery<CommentInput>,
    update: UpdateQuery<CommentInput>,
    options: QueryOptions
) {
    return CommentModel.findOneAndUpdate(query, update, options);
}

export async function deleteComment(query: FilterQuery<CommentDocument>) {
    return CommentModel.deleteOne(query);
}
