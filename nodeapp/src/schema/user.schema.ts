import { object, string, TypeOf, optional } from "zod";

export const createUserSchema = object({
    body: object({
        name: string({
            required_error: "Name is required",
        }).min(3, "Name must be at least 3 characters"),
        password: string({
            required_error: "Password is required",
        }).min(6, "Password too short - should be 6 chars minimum"),
        passwordConfirmation: string({
            required_error: "PasswordConfirmation is required",
        }),
        username: string({
            required_error: "Username is required",
        }).min(3, "Username must be at least 3 characters"),
        email: string({
            required_error: "Email is required",
        }).email("Not a valid email"),
    }).refine((data) => data.password === data.passwordConfirmation, {
        message: "Passwords do not match",
        path: ["passwordConfirmation"],
    }),
});

export const updateUserSchema = object({
    body: object({
        name: optional(string().min(2, "Name must be at least 2 characters")),
        password: optional(string().min(6, "Password too short - should be 6 chars minimum")),
        username: optional(string().min(2, "Username must be at least 2 characters")),
        email: optional(string().email("Not a valid email")),
    }),
});


export type UpdateUserInput = Partial<TypeOf<typeof updateUserSchema>["body"]>;

export type CreateUserInput = Omit<
    TypeOf<typeof createUserSchema>,
    "body.passwordConfirmation"
>;
