import { object, number, string, TypeOf } from "zod";

const body = {
    body: object({
        comment: string({
            required_error: "Comment is required",
        }),
    }),
};

const params = {
    params: object({
        postId: string({
            required_error: "postId is required",
        }),
        commentId: string({
            required_error: "commentId is required",
        }),
    }),
};

const params2 = {
    params: object({
        postId: string({
            required_error: "postId is required",
        }),
    }),
};
export const createCommentSchema = object({
    ...body,
    ...params2
});

export const updateCommentSchema = object({
    ...body,
    ...params,
});

export const deleteCommentSchema = object({
    ...params,
});

export const getCommentSchema = object({
    ...params,
});

export type CreateCommentInput = TypeOf<typeof createCommentSchema>;
export type UpdateCommentInput = TypeOf<typeof updateCommentSchema>;
export type ReadCommentInput = TypeOf<typeof getCommentSchema>;
export type DeleteCommentInput = TypeOf<typeof deleteCommentSchema>;
