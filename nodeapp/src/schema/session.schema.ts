import { object, string } from "zod";

export const createSessionSchema = object({
    body: object({
        email: string().optional(),
        username: string()
            .min(2, {
                message: "Username must be at least 3 characters",
            })
            .optional(),
        password: string().min(6, {
            message: "Password must be at least 6 characters",
        }),
    }).refine(
        (data) => {
            if (!data.email && !data.username) {
                throw new Error(
                    "At least one of email or username is required"
                );
            }
            return true;
        },
        { message: "At least one of email or username is required" }
    ),
});
