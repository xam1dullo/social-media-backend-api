import { object, number, string, TypeOf } from "zod";

const body = {
    body: object({
        title: string({
            required_error: "Title is required",
        }),
        body: string({
            required_error: "Body is required",
        }),
    }),
};


const upBody = {
    body: object({
        title: string().optional(),
        body: string().optional(),
    }),
};
const params = {
    params: object({
        postId: string({
            required_error: "postId is required",
        }),
    }),
};

export const createPostSchema = object({
    ...body,
});

export const updatePostSchema = object({
    ...upBody,
    ...params,
});

export const deletePostSchema = object({
    ...params,
});

export const getPostSchema = object({
    ...params,
});

export type CreatePostInput = TypeOf<typeof createPostSchema>;
export type UpdatePostInput = TypeOf<typeof updatePostSchema>;
export type ReadPostInput = TypeOf<typeof getPostSchema>;
export type DeletePostInput = TypeOf<typeof deletePostSchema>;
