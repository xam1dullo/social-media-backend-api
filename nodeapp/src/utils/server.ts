import express, { Request, Response } from "express";
import rateLimit from "express-rate-limit";
import config from "config";
import responseTime from "response-time";
import deserializeUser from "../middleware/deserializeUser";
import routes from "../routes";
import { restResponseTimeHistogram, startMetricsServer } from "./metrics";
import logger from "./logger";

const windowMs: number = config.get<number>("RATE_LIMIT_MAX_REQUESTS");
const max: number = config.get<number>("RATE_LIMIT_WINDOW_MS");


let limiter = rateLimit({
    windowMs: 60 * 1000,
    max: 100,
    standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});

function createServer() {
    const app = express();
    app.use(express.json());
    app.use(deserializeUser);
    app.use(limiter)
    app.use(
        responseTime(
            (req: Request, res: Response, time: number) => {
                if (req?.route?.path) {
                    restResponseTimeHistogram.observe(
                        {
                            method: req.method,
                            route: req.route.path,
                            status_code: res.statusCode,
                        },
                        time * 1000
                    );
                }
            })
    );
    routes(app);
    return app;
}

export default createServer;
