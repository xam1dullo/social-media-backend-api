import { Express, Request, Response, Router } from "express";
import rateLimit from "express-rate-limit";
import config from "config";
import commentRoutes from "./routes/comment.routes";
import userRoutes from "./routes/user.routes";
import postRoutes from "./routes/post.routes";
import sessionRoutes from "./routes/session.routes";
import healthcheckRoutes from "./routes/healthcheck.routes";

import { restResponseTimeHistogram, startMetricsServer } from "./utils/metrics";


const windowMs: number = config.get<number>("RATE_LIMIT_MAX_REQUESTS");
const max: number = config.get<number>("RATE_LIMIT_WINDOW_MS");

let limiter = rateLimit({
    windowMs,
    max,
    standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});

console.log({
    limiter,
    windowMs,
    max
})
function routes(app: Express) {

    app.use("/api", limiter);
    app.use("/api/users", userRoutes);

    app.use("/api/sessions", sessionRoutes);

    app.use("/api/posts", postRoutes);

    app.use("/api/posts/:postId/comments", commentRoutes);

    app.use("/api/healthcheck", healthcheckRoutes);

}

export default routes;
