import { Request, Response } from "express";
import { 
    CreateUserInput, 
    UpdateUserInput, 
} from "../schema/user.schema";
import { 
    createUser,
    validatePassword,
    findUser,
    updateUser,
    deleteUser,
} from "../service/user.service";
import logger from "../utils/logger";

export async function createUserHandler(
    req: Request<{}, {}, CreateUserInput["body"]>,
    res: Response
) {
    try {
        const body = req.body;

        const user = await createUser(req.body);
        return res.send(user);
    } catch (e: any) {
        logger.error(e);
        return res.status(409).send(e.message);
    }
}

export async function getUserHandler(
    req: Request,
    res: Response
) {
    try {
        const userId = res.locals.user._id;

        const user = await findUser({ userId }, {}, {});
        return res.send(user);
    } catch (e: any) {
        logger.error(e);
        return res.status(404).send(e.message);
    }
}

export async function updateUserHandler(
    req: Request,
    res: Response
) {
    const userId = req.params.userId;
    try {
        const user = await updateUser({ userId }, { $set: req.body });
        return res.send(user);
    } catch (e: any) {
        logger.error(e);
        return res.status(404).send(e.message);
    }
}

export async function deleteUserHandler(
    req: Request,
    res: Response
) {
    const userId = req.params.userId;
    try {
        await deleteUser({ userId });
        return res.sendStatus(204);
    } catch (e: any) {
        logger.error(e);
        return res.status(404).send(e.message);
    }
}
