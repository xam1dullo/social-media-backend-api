import { Request, Response } from "express";
import {
    CreateCommentInput,
    UpdateCommentInput,
    ReadCommentInput,
    DeleteCommentInput,
} from "../schema/comment.schema";
import {
    createComment,
    findComment,
    findAndUpdateComment,
    deleteComment,
    findComments
} from "../service/comment.service";

export async function createCommentHandler(
    req: Request,
    res: Response
) {
    const userId = res.locals.user._id;

    const body = req.body;
    const postId = req.params.postId!; // Get postId parameter here
    const comment = await createComment({
        ...body,
        user: userId,
        postId,
    });

    return res.send(comment);
}


export async function updateCommentHandler(
    req: Request<UpdateCommentInput["params"]>,
    res: Response
) {
    const userId = res.locals.user._id;

    const { postId, commentId } = req.params;

    const update = req.body;

    const comment = await findComment({ postId, commentId, userId });

    if (!comment) {
        return res.sendStatus(404);
    }

    if (String(comment.user) !== userId) {
        return res.sendStatus(403);
    }

    const updatedComment = await findAndUpdateComment(
        { postId, commentId, userId },
        update,
        {
            new: true,
        }
    );

    return res.send(updatedComment);
}

export async function getCommentHandler(
    req: Request<ReadCommentInput["params"]>,
    res: Response
) {
    const userId = res.locals.user._id;
    const { postId, commentId } = req.params;

    const comment = await findComment({ postId, commentId, userId });

    if (!comment) {
        return res.sendStatus(404);
    }

    return res.send(comment);
}

export async function getCommentsHandler(
    req: Request<ReadCommentInput["params"]>,
    res: Response
) {
    const userId = res.locals.user._id;
    const { postId } = req.params;

    const page = parseInt(req.query.page as string) || 1;
    const limit = parseInt(req.query.limit as string) || 100;

    const comment = await findComments(postId, page
        , limit)

    if (!comment) {
        return res.sendStatus(404);
    }

    return res.send(comment);
}

export async function deleteCommentHandler(
    req: Request<DeleteCommentInput["params"]>,
    res: Response
) {
    const userId = res.locals.user._id;
    const { postId, commentId } = req.params;

    const product = await findComment({ postId, commentId, userId });

    if (!product) {
        return res.sendStatus(404);
    }

    if (String(product.user) !== userId) {
        return res.sendStatus(403);
    }

    await deleteComment({ postId, commentId, userId });

    return res.sendStatus(200);
}
