import { Request, Response } from "express";
import {
    CreatePostInput,
    UpdatePostInput,
    ReadPostInput,
    DeletePostInput,
} from "../schema/post.schema";
import {
    createPost,
    findPost,
    findAndUpdatePost,
    deletePost,
    findPosts,
} from "../service/post.service";

export async function createPostHandler(
    req: Request<{}, {}, CreatePostInput["body"]>,
    res: Response
) {
    const userId = res.locals.user._id;

    const body = req.body;

    const post = await createPost({
        ...body,
        user: userId,
    });

    return res.send(post);
}

export async function updatePostHandler(
    req: Request<UpdatePostInput["params"], UpdatePostInput["body"]>,
    res: Response
) {
    const userId = res.locals.user._id;

    const postId = req.params.postId;
    const update = req.body;

    const post = await findPost({ postId });

    if (!post) {
        return res.sendStatus(404);
    }

    if (String(post.user) !== userId) {
        return res.sendStatus(403);
    }
    const updatedPost = await findAndUpdatePost({ postId, userId }, { $set: update }, {
        new: true,
    });

    return res.send(updatedPost);
}

export async function getPostsHandler(req: Request, res: Response) {
    const page = parseInt(req.query.page as string) || 1;
    const limit = parseInt(req.query.limit as string) || 100;
    
    const posts = await findPosts(page, limit);

    return res.send(posts);
}

export async function getPostHandler(
    req: Request<ReadPostInput["params"]>,
    res: Response
) {
    const postId = req.params.postId;
    const post = await findPost({ postId });

    if (!post) {
        return res.sendStatus(404);
    }

    return res.send(post);
}

export async function deletePostHandler(
    req: Request<DeletePostInput["params"]>,
    res: Response
) {
    const userId = res.locals.user._id;
    const postId = req.params.postId;

    const post = await findPost({ postId, userId });

    if (!post) {
        return res.sendStatus(404);
    }

    if (String(post.user) !== userId) {
        return res.sendStatus(403);
    }

    await deletePost({ postId });

    return res.sendStatus(200);
}
