
# Social Media Backend API

This is a backend API for a social media application that allows users to perform CRUD operations on posts and comments. It is built using Node.js and Express.js, with MongoDB as the database.
Installation

 Clone the repository:

```bash

    git clone https://gitlab.com/xam1dullo/social-media-backend-api.git
    cd ./social-media-backend-api
    cd ./nodeapp
```
Postman

[Documentation]( https://documenter.getpostman.com/view/15905030/2s93eeQUbq) or Import postman collection.json file  [link](social-media-backend-api.postman_collection.json)    




Install the dependencies:

```bash
    yarn install
```

Start the server dev:

```bash
    yarn dev
```

    The server should now be running at http://localhost:8080.


###  Docker Compose: The application can be packaged using Docker Compose. Use the following commands to build and run the Docker image:
```bash
    docker-compose up 
 ```
 ```bash
    docker-compose up -d 
 ```

API Endpoints
Authentication

    POST /api/users: Creates a new user account.
    GET /api/users: Get a user.
    PUT /api/users: Update a user.
    DELTE /api/users: Delete a user.

Session

    POST /api/sessions: Creates a new session.
    GET /api/sessions/: Returns a list of all sessions.
    PUT /api/sessions/: Updates the session.
    DELETE /api/sessions/: Deletes the session.


Posts

    GET /api/posts: Returns a list of all posts.
    GET /api/posts/:id: Returns the post with the specified ID.
    POST /api/posts: Creates a new post.
    PUT /api/posts/:id: Updates the post with the specified ID.
    DELETE /api/posts/:id: Deletes the post with the specified ID.

Comments

    GET /api/posts/:postId/comments: Returns a list of all comments for the post with the specified ID.
    POST /api/posts/:postId/comments: Creates a new comment for the post with the specified ID.
    PUT /api/posts/:postId/comments/:id: Updates the comment with the specified ID for the post with the specified post ID.
    DELETE /api/posts/:postId/comments/:id: Deletes the comment with the specified ID for the post with the specified post ID.



-    http://localhost:9100/metrics  Returns the Prometheus metrics for the REST API response time and database response time.


 -   Pagination: The GET /api/posts and GET /api/posts/:postId/comments endpoints support pagination. The default page size is 10, but this can be customized using the limit query parameter.

  -  Rate Limiting: The API endpoints are rate limited using the express-rate-limit middleware. The rate limit is set to 100 requests per minute, but this can be customized using the RATE_LIMIT_WINDOW_MS and RATE_LIMIT_MAX_REQUESTS environment variables.

 
